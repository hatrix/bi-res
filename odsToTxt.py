﻿from xml.dom import minidom
import zipfile

xml = zipfile.ZipFile("biere.ods").read('content.xml')

xmldoc = minidom.parseString(xml)
itemlist = xmldoc.getElementsByTagName('table:table-row')[1:]

f = open("eval.txt", "w")

j = 0
for node in itemlist:
    beers = node.getElementsByTagName('table:table-cell')
   
    for i in range(len(beers)):
        if i > 4:
            break;
            
        tmp = beers[i].getElementsByTagName('text:p')
        if len(tmp) > 0:  
            if i == 4:
                eval = "Mauvaise"
            elif i == 3:
                eval = "Moyenne -"
            elif i == 2:
                eval = "Moyenne"
            elif i == 1:
                eval = "Moyenne +"
            elif i == 0:
                eval = "Bonne"
                
            f.write(eval + ",")
            f.write(tmp[0].firstChild.nodeValue + "\n")
            
            j += 1

print("{} bières".format(j))
