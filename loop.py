from search import Beer
import io

f = open("eval.txt", "r")
data = f.readlines()

cats = ["Bonnes :", "Moyennes - :", "Moyennes :", "Moyennes + :", 
        "Mauvaises :"]
current = ""
for beer in data:
    current = beer.split(",")[0]
    
    name = beer.split(",")[1].strip()
    print("Searching for {}...".format(name))
    
    search_beer = Beer(name)
    
    try:
        search_beer.search()
        search_beer.write(current)
    except:
        print("Unable to find beer '{}'".format(name))
        
        f = io.open('beers.txt', 'a', encoding='utf8')
        f.write('Name: \t\t' + name + '\n')
        f.write('Style:\n')
        f.write('Alcohol:\n')
        f.write('Calories:\n')
        f.write(u'Score:\n')
        f.write('Glasses:\n')
        f.write('Perso: \t\t' + current + '\n')
        f.write('Link:\n')
        f.write('Image:\n')
        f.write(u'\n')
        f.close()
