from html.parser import HTMLParser
import requests
import sys
import io

class Beer:
    def __init__(self, search):
        self.search_string = search

    def search(self):
        beer = self.search_string
        # Search the beer
        url = "http://www.ratebeer.com/advbeersearch.php"

        playload = {'BeerName': beer, 'SortBy': 1, 'BeerStyles': 0,
                    'submit1': 'Search'}
        r = requests.post(url, data=playload)

        data = r.text.split("<!-- Content begins -->")[1]
        data = data.split("<!-- Content ends -->")[0]

        # Get url of the first found beer
        url = data.split('<A HREF="')[1].split('">')[0]
        url = "http://www.ratebeer.com" + url

        # Open the url of the beer
        r = requests.get(url)

        # Get proper name
        name = HTMLParser()
        name = name.unescape(r.text.split('<TITLE>')[1].split('</TITLE>')[0])

        # Get image
        cloud = 'http://res.cloudinary.com/ratebeer/image/upload'
        image = r.text.split('<a href="' + cloud)[1]
        image = cloud + image.split('">')[0]

        # Get style
        style = HTMLParser()
        style = style.unescape(r.text.split('<br>Style: <a href="')[1])
        style = style.split('">')[1]
        style = style.split('</a>')[0]

        # Get alcohol percentage
        abv = r.text.split('ABV</abbr>: <big style="color: #777;"><strong>')[1]
        abv = abv.split('</strong>')[0]

        # Get calories (per 33cl)
        calories = r.text.split('EST. CALORIES')[1].split(';">')[1]
        calories = calories.split('</strong>')[0]

        # Get overall score
        try:
            score = r.text.split('overall</span><br><span')[1]
            score = score.split('</span>')[0]
            score = score.split('>')[1]
        except:
            score = "x"

        # Get glass type
        glasses = r.text.split('Serve in')[1].split('</a></div>')[0]
        glasses = glasses.split('</a>')
        glasses = [t.split('">')[-1] for t in glasses]
        
        self.url = url
        self.image = image
        self.name = name
        self.style = style
        self.abv = abv
        self.calories = calories
        self.score = score
        self.glasses = glasses

    def print_att(self):
        print('Name: \t\t' + self.name)
        print('Style: \t\t' + self.style)
        print('Alcohol: \t' + self.abv)
        print('Calories: \t' + self.calories + ' per 33cl')
        print('Score: \t\t' + self.score + "/100")
        print('Glasses: \t' + ", ".join(self.glasses))
        print('Link: \t' + self.url)
        print('Image: \t' + self.image)
        print(self.url)
        print(self.image)

    
    def write(self, eval=""):
        f = io.open('beers.txt', 'a', encoding='utf8')
        f.write('Name: \t\t' + self.name + '\n')
        f.write('Style: \t\t' + self.style + '\n')
        f.write('Alcohol: \t' + self.abv + '\n')
        f.write('Calories: \t' + self.calories + ' per 33cl\n')
        f.write(u'Score: \t\t' + self.score + "/100" + '\n')
        f.write('Glasses: \t' + ", ".join(self.glasses) + '\n')
        f.write('Perso: \t\t' + eval + '\n')
        f.write('Link: \t\t' + self.url + '\n')
        f.write('Image: \t\t' + self.image + '\n')
        f.write(u'\n')
        f.close()

if __name__ == "__main__":
    if len(sys.argv) > 1:
        beer = " ".join(sys.argv[1:])
    else:
        beer = "leffe blonde"
    thirsty = Beer(beer)
    thirsty.search()
    thirsty.print_att()
