from urllib import request
import os

f = open("static/beers.txt", 'r')

for line in f:
    if line.startswith('Image'):
        image = line.split('Image:')[1].strip()
        name = image.split('/')[-1]
        
        if image:
            if name not in os.listdir('static/images'):
                f = open('static/images/' + name, 'wb')
                f.write(request.urlopen(image).read())
                f.close()
