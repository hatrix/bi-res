﻿# -*- coding: utf-8 -*-

from flask import render_template, request, redirect, url_for
from beers import app
from pytz import timezone
from collections import OrderedDict

app.jinja_env.trim_blocks = True

STATIC = "/home/hatrix/git/bieres/site/beers/static/"

@app.errorhandler(404)
def page_not_found(e):
    return "404"

@app.route('/')
@app.route('/index')
def index():
    blist = readBeers()
    return render_index(blist)

def render_index(blist):
    return render_template("index.html", blist=blist)

@app.route('/alphabetical')
def alphabetical():
    blist = readBeers()
    blist = sorted(blist, key=lambda x: x['name'])
    return render_index(blist)

@app.route('/score')
def score():
    blist = readBeers()
    blist = sorted(blist, 
            key=lambda x: int(x['score'][0:-4])
                    if x['score'] != "" and x['score'][0:-4] != 'x' else 0, 
            reverse=True)

    return render_index(blist)

@app.route('/alcohol')
def alcohol():
    blist = readBeers()
    blist = sorted(blist, 
            key=lambda x: float(x['alcohol'][0:-1])
                if x['alcohol'] != "" else 0, 
            reverse=True)

    return render_index(blist)

@app.route('/calories')
def calories():
    blist = readBeers()
    blist = sorted(blist,
            key=lambda x: int(x['calories'][0:-9])
                if x['calories'] != "" else 0,
            reverse = True)
    
    return render_index(blist)

@app.route('/perso')
def perso():
    blist = readBeers()
    blist = sorted(blist,
            key=lambda x: 0 if x['perso'] == "Bonne"
            else 1 if x['perso'] == "Moyenne +"
            else 2 if x['perso'] == "Moyenne"
            else 3 if x['perso'] == "Moyenne -"
            else 4 if x['perso'] == "Mauvaise"
            else 5)

    return render_index(blist)

def readBeers():
    beers = open(STATIC + "beers.txt", "r").readlines()
   
    blist = []
    b = dict()
    for line in beers:
        name = style = alcohol = calories = score = glasses = perso = ""
        
        if line.startswith("Name"):
            b['name'] = bytes(line.split("Name:")[1].strip()).decode('utf8')
            
        elif line.startswith("Style"):
            b['style'] = bytes(line.split("Style:")[1].strip()).decode('utf8')
           
        elif line.startswith("Alcohol"):
            b['alcohol'] = bytes(line.split("Alcohol:")[1].strip()).decode('utf8')
            
        elif line.startswith("Calories"):
            b['calories'] = bytes(line.split("Calories:")[1].strip()).decode('utf8')
            
        elif line.startswith("Score"):
            b['score'] = line.split("Score:")[1].strip()
            
        elif line.startswith("Glasses"):
            b['glasses'] = bytes(line.split("Glasses:")[1].strip()).decode('utf8')
            
        elif line.startswith("Perso"):
            b['perso'] = line.split("Perso:")[1].strip()
            
        elif line.startswith("Link"):
            b['link'] = bytes(line.split("Link:")[1].strip()).decode('utf8')
            
        elif line.startswith("Image"):
            b['image'] = bytes(line.split("Image:")[1].strip()).decode('utf8')
            b['image'] = b['image'].split('/')[-1]
       
            blist.append(b)
            b = dict()

    return blist
